
 $(document).ready(function () {
 $("#flightsTable").hide();
 $("#flightBookingForm").submit(function (event) {

 //stop submit the form, we will post it manually.
 event.preventDefault();

 //searchFlights();
 fire_ajax_submit();

 });
 });

function makeTable(data) {
var rows = '';
$.each(data, function(rowIndex, r) {
var row = "<tr>";
row += "<td>"+r.flightNo+"</td>"
row += "<td>"+r.flightName+"</td>";
row += "<td>"+r.origin+"</td>";
row += "<td>"+r.destination+"</td>";
row += "<td>"+r.departureDate+"</td>";
row += "<td>"+r.TravelClass+"</td>";
//row += "<td>"+r.totalFare+"</td>";
row += "</tr>";
rows += row;
});
return rows;
}


function fire_ajax_submit() {

$.ajax({
//type: "POST",
//url: "/flight/searchflights",
type: "GET",
url: "/flight/availableflights",
data: {
flightNo: $("#flightNo").val,
flightName: $("flightName").val,
source: $("#source").val(),
destination: $("#destination").val(),

//numberOfPassengers:$("#passengerCount").val(),
//date:$("#departureDate").val()
passengerCount: $("#passengerCount").val(),
departureDate: $("#departureDate").val(),
travelType: $("#travelClass").val()
//flightBookingForm["numberOfPassengers"] = $("#numberOfPassengers").val();
},
dataType: 'json',
timeout: 800000,
success: function(data) {
console.log("SUCCESS : ", data);
$("#flightsTable").show();
if(data!=null && data.length>0)
{
var rows = makeTable(data);
$('#flightRows').html(rows);
}
else
{
$("#flightRows").text('No flights found.');
}
var json = "<h4>Ajax Response</h4><pre>"+ JSON.stringify(data, null, 4) + "</pre>";
//$('#feedback').html(json);
},
error: function (e) {
console.log("ERROR : ", e);
var json = "<h4>Ajax Response</h4><pre>"+ e.responseText + "</pre>";
$('#feedback').html(json);
}
});

}
