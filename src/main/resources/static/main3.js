$(document).ready(function () {
   $("#flightBookingForm").submit(function (event) {
       //stop submit the form, we will post it manually.
       event.preventDefault();
       fire_ajax_submit();
   });
   //jQuery Datepicker
   // Datepicker Popups calender to Choose date.
   $(function() {
       $("#departureDate").datepicker();
       // Pass the user selected date format.
       //$("#format").change(function() {
         //  $("#datepicker").datepicker("option", "dateFormat", $(this).val());
           //alert(''+($(this).val()));
       //});
   });
});
function fire_ajax_submit() {
   var search = {}
   search["origin"] = $("#origin").val();
   search["destination"] = $("#destination").val();
   search["departureDate"] = $("#departureDate").val();
   //search["flightNo"] = $("#flightNo").val();
   search["noOfSeats"] = $("#noOfSeats").val();
   //search["flightName"] = $("#flightName").val();
   //search["departureDate"] = $("#departureDate").val();
   $("#btn-search").prop("disabled", true);
   $.ajax({
       type: "POST",
       contentType: "application/json",
       url: "/flight/searchflights",
       data: JSON.stringify(search),
       dataType: 'json',
       cache: false,
       timeout: 600000,
       success: function (data) {
           var json = "<h4>Ajax Response </h4><pre>";
            json+ = JSON.stringify(data, null, 4) ;
          var json2 = data.FlightSearchResponse;
               $('#flightsTable').closest('tr').remove();
               if(data.FlightSearchResponse!=null && data.FlightSearchResponse.length>0){
               $('#feedback').hide();
                   //create table rows
                                   var newRow = "";
                                   var cols = "";
                                   var i=1;
                                   for(x in json2){
                                           //alert(json2[x].flightName);
                                           newRow = $("<tr id='f"+i+"'>");
                                           cols += '<td>'+(i++)+'</td>';
                                           cols += '<td>'+json2[x].flightName+'</td>';
                                           cols += '<td>'+json2[x].origin+'</td>';
                                           cols += '<td>'+json2[x].destination+'</td>';
                                           cols += '<td><a href="#">Show</a></td>';
                                           newRow.append(cols);
                                           $('#flightsTable').append(newRow);
                                           cols="";
                                           newRow="";
                                       }
                   //create table row ends

                    $('#feedback').html(json);
              }else{
                      $('#feedback').html(json);
               }
               //console.log("SUCCESS : ", data.flightObjectList);
               console.log("SUCCESS : ", data.FlightSearchResponse);

               $("#btn-search").prop("disabled", false);
       },
       error: function (e) {
           var json = "<h4>Ajax Response</h4><pre>"
               + e.responseText + "</pre>";
           $('#feedback').html(json);
           console.log("ERROR : ", e);
           $("#btn-search").prop("disabled", false);
       }
   });
}