
$(document).ready(function () {
 $("#flightsTable").hide();
 $("#flightBookingForm").submit(function (event) {

 //stop submit the form, we will post it manually.
 event.preventDefault();

 //searchFlights();
 fire_ajax_submit();

 });
 });

function makeTable(data) {
var rows = '';
$.each(data, function(rowIndex, r) {
var row = "<tr>";
row += "<td>"+r.flightNo+"</td>"
row += "<td>"+r.flightName+"</td>";
row += "<td>"+r.origin+"</td>";
row += "<td>"+r.destination+"</td>";
//row += "<td>"+r.departureDate+"</td>";
//row += "<td>"+r.TravelClass+"</td>";
//row += "<td>"+r.totalFare+"</td>";
row += "</tr>";
rows += row;
});
return rows;
}

function fire_ajax_submit() {

    var search = {}
    //search["username"] = $("#username").val();
//    data: {
//    ["flightNo"] = $("#flightNo").val(),
//    ["flightName"] = $("#flightName").val(),
//    ["source"] = $("#source").val(),
//     ["destination"] = $("#destination").val(),
//     ["date"] = $("#date").val(),
//      },

    $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/flight/searchflights",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
        console.log("SUCCESS : ", data);
        $("#flightsTable").show();
        if(data!=null && data.length>0)
        {
        var rows = makeTable(response);
        $('#flightRows').html(rows);
        }
        else
        {
        $("#flightRows").text('No flights found.');
        }
            var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#feedback').html(json);

            console.log("SUCCESS : ", data);
            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}